import React, { Fragment, useState, useEffect } from 'react';

import axios from 'axios';

import Form from './components/Form';
import Cancion from './components/cancion';
import Informacion from './components/Informacion'

function App() {
	//Definir el state
	const [busquedaletra, guardarBusquedaLetra] = useState({});
	const [letra, guardarLetra] = useState('');
	const [informacion, guardarInformacion] = useState({});

	useEffect(() => {
		//Si el objeto busquedaletra esta vacio
		if (Object.keys(busquedaletra).length === 0) return;

		const consultarAPI = async () => {
			const { artista, cancion } = busquedaletra;
			const urlAPI = `https://api.lyrics.ovh/v1/${artista}/${cancion}`;
      const urlAPI2 = `https://www.theaudiodb.com/api/v1/json/1/search.php?s=${artista}`;
      
      //con este codigo empezamos al mismo tiempo las peticiones
			const [letra, informacion] = await Promise.all([
				axios.get(urlAPI),
				axios.get(urlAPI2),
      ]);
      console.log(letra.data.lyrics);
      guardarLetra(letra.data.lyrics);
      guardarInformacion(informacion.data.artists[0]);
      //guardarInformacion(informacion.artist);
		};
		consultarAPI();

		// CON ESTA LINEA EVITAMOS LOOP
		guardarBusquedaLetra({});
	}, [busquedaletra, informacion]);

	return (
		<Fragment>
			<Form guardarBusquedaLetra={guardarBusquedaLetra} />
			<div className="container mt-5">
				<div className="row">
					<div className="col-md-6">
						<Informacion informacion={informacion} />
					</div>
					<div className="col-md-6">
						<Cancion letra={letra} />
					</div>
				</div>
			</div>
		</Fragment>
	);
}

export default App;
