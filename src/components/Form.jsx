import React, { useState } from 'react';

import Error from './Error';
const Form = ({ guardarBusquedaLetra }) => {
	const [busqueda, guardarBusqueda] = useState({
		artista: '',
		cancion: '',
	});
	const [error, guardarError] = useState(false);
	const { artista, cancion } = busqueda;

	// Funcion para cada input para leer su contenido
	const actualizarState = (e) => {
		guardarBusqueda({
			...busqueda,
			[e.target.name]: e.target.value,
		});
	};

	const buscarCancion = (e) => {
		e.preventDefault();

		//Validacion del form
		if (cancion.trim() === '' || artista.trim() === '') {
			guardarError(true);
			return;
		}
		//quitamos el error de validacion
        guardarError(false);
        
        guardarBusquedaLetra(busqueda)
	};

	return (
		<div className="bg-info">
			{error ? <Error mensaje="Todos los campos son obligatorios" /> : null}
			<div className="container">
				<div className="row">
					<form
						onSubmit={buscarCancion}
						className="col card text-white bg-transparent mb-5 pt-5 pb-2"
					>
						<fieldset>
							<legend className="text-center">Buscador Letras Canciones</legend>

							<div className="row">
								<div className="col-md-6">
									<div className="form-group">
										<label htmlFor="">Artista</label>
										<input
											type="text"
											className="form-control"
											name="artista"
											placeholder="Nombre del artista"
											onChange={actualizarState}
											value={artista}
										/>
									</div>
								</div>
								<div className="col-md-6">
									<div className="form-group">
										<label htmlFor="">Canción</label>
										<input
											type="text"
											className="form-control"
											name="cancion"
											placeholder="Nombre del artista"
											onChange={actualizarState}
											value={cancion}
										/>
									</div>
								</div>
							</div>

							<button type="submit" className="btn btn-primary float-right">
								Buscar
							</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	);
};

export default Form;
