import React, { Fragment } from 'react';

const Cancion = ({ letra }) => {
	return letra ? (
		<Fragment>
			<h2>Letra Canción</h2>
			<p className="letra">{letra}</p>
		</Fragment>
	) : null;
};

export default Cancion;
